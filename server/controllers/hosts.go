package controllers

import (
	"errors"
	"fmt"
	hostmgr "youbei/utils/host"

	"youbei/dao"

	"github.com/gin-gonic/gin"
)

func HostAdd(c *gin.Context) {
	ob := new(dao.Host)
	if err := c.Bind(ob); err != nil {
		APIReturn(c, 500, "解析录入失败", err.Error())
		return
	}

	if err := hostmgr.AddhostServer(ob); err != nil {
		APIReturn(c, 500, "录入主机失败1", err.Error())
		return
	}

	if err := ob.Add(); err != nil {
		APIReturn(c, 500, "录入主机失败", err.Error())
		return
	}

	APIReturn(c, 200, "录入成功", nil)
}

func HostUpdate(c *gin.Context) {
	id := c.Param("id")
	ob := new(dao.Host)
	if err := c.Bind(ob); err != nil {
		APIReturn(c, 500, "解析修改失败", err.Error())
		return
	}
	ob.ID = id
	if err := ob.Update(); err != nil {
		APIReturn(c, 500, "修改主机失败", err.Error())
		return
	}
	APIReturn(c, 200, "修改成功", nil)
}

func HostDelete(c *gin.Context) {
	id := c.Param("id")
	ob := new(dao.Host)
	ob.ID = id
	if err := ob.Delete(); err != nil {
		APIReturn(c, 500, "删除主机失败", err.Error())
		return
	}
	APIReturn(c, 200, "删除成功", nil)
}

func HostsGet(c *gin.Context) {
	rep := map[string]interface{}{}
	hosts := []dao.Host{}
	if total, err := GetRestul(c, "hostlist", &hosts); err != nil {
		APIReturn(c, 500, "获取列表失败", err.Error())
		return
	} else {
		rep["count"] = total
	}
	rep["data"] = hosts
	APIReturn(c, 200, "获取列表成功", &rep)
}

func HostGet(c *gin.Context) {
	id := c.Param("id")
	host := new(dao.Host)
	host.ID = id
	fmt.Println(id)
	if bol, err := dao.Localdb().ID(id).Get(host); err != nil {
		APIReturn(c, 500, "获取数据失败", err.Error())
		return
	} else {
		if !bol {
			APIReturn(c, 500, "数据不存在", errors.New("数据不存在"))
			return
		}
	}
	APIReturn(c, 200, "获取数据成功", host)
}
