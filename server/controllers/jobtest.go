package controllers

import (
	"youbei/utils/jobs"

	"github.com/gin-gonic/gin"
)

func RunJob(c *gin.Context) {
	if err := jobs.Backup(c.Param("id")); err != nil {
		APIReturn(c, 500, "执行失败", err)
		return
	}
	APIReturn(c, 200, "执行成功", nil)
}
