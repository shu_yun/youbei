package controllers

import (
	"youbei/dao"
	"youbei/utils/httpFunc"

	"github.com/gin-gonic/gin"
)

func Proxy(c *gin.Context) {
	id := c.Param("hostid")
	host, err := dao.GetHost(id)
	if err != nil {
		APIReturn(c, 500, "查询主机失败", err)
		return
	}
	httpFunc.Proxy(c, host.HostAddr+":"+host.Port+c.Request.URL.String(), host.Token)
	return
}
