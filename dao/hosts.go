package dao

import (
	"errors"
	"github.com/segmentio/ksuid"
)

type Request struct {
	Uuid string `json:"uuid"`
	Msg  string `json:"msg,omitempty"`
}

type Response struct {
	Status int    `json:"status"` // 200表示允许，其他表示不允许
	Msg    string `json:"msg"`
}

type Host struct {
	ID       string `json:"id" xorm:"pk notnull unique 'id'"`
	Protocol string `json:"protocol" xorm:"'protocol'"`
	Name     string `json:"name" xorm:"'name'"`
	HostAddr string `json:"hostaddr" xorm:"'hostaddr'"`
	HostUuid string `json:"hostuuid" xorm:"'hostuuid'"`
	Port     string `json:"port" xorm:"'port'"`
	Created  int64  `json:"created" xorm:"'created'"`
	Token    string `json:"token" xorm:"'token'"`
}

func (c *Host) Add() error {
	c.ID = ksuid.New().String()
	c.Created = NowTime()
	_, err := localdb.Insert(c)
	return err
}

func GetHost(id string) (*Host, error) {
	host:=new(Host)
	if bol,err:=localdb.ID(id).Get(host);err!=nil{
		return host,err
	}else{
		if !bol{
			return host,errors.New("获取为空")
		}
	}
	return host,nil
}

func (c *Host) Update() error {
	c.Created = NowTime()
	_, err := localdb.ID(c.ID).Cols("name", "hostaddr", "port").Update(c)
	return err
}

func (c *Host) Delete() error {
	_, err := localdb.ID(c.ID).Delete(new(Host))
	return err
}
