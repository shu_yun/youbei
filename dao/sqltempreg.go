package dao

func initSqlTempReg() {
	loglist := "select * from `log` as a left join `task` as b on a.`tid`=b.`id` where a.`id`!=''" +
		"{% if name %}" +
		" and a.`name` like '%{{name}}%' " +
		"{% endif %}" +
		"{% if taskname %}" +
		" and b.`name` like '%{{taskname}}%' " +
		"{% endif %}" +
		"{% if dbname %}" +
		" and b.`dbname` like '%{{dbname}}%' " +
		"{% endif %}" +
		" group by a.`id` order by a.`created` desc " +
		"{% if pageSize && currentPage%}" +
		" limit {{pageSize*(currentPage-1)}},{{pageSize}} " +
		"{% endif %}"
	if err := localdb.SqlTemplate.AddSqlTemplate("loglist", loglist); err != nil {
		panic(err)
	}

	tasklist := "select * from `task` where `id`!=''" +
		"{% if name %}" +
		" and `name` like '%{{name}}%' " +
		"{% endif %}" +
		"{% if dbname %}" +
		" and `dbname` like '%{{dbname}}%' " +
		"{% endif %}" +
		"{% if dbtype %}" +
		" and `dbtype`=='{{dbtype}}' " +
		"{% endif %}" +
		" order by `created` desc " +
		"{% if pageSize && currentPage%}" +
		" limit {{pageSize*(currentPage-1)}},{{pageSize}} " +
		"{% endif %}"

	if err := localdb.SqlTemplate.AddSqlTemplate("tasklist", tasklist); err != nil {
		panic(err)
	}

	crontablist := "select * from `crontab` where `id`!=''" +
		"{% if name %}" +
		" and `name` like '%{{name}}%'" +
		"{% endif %}" +
		"{% if pageSize && currentPage%}" +
		" limit {{pageSize*(currentPage-1)}},{{pageSize}} " +
		"{% endif %}"

	if err := localdb.SqlTemplate.AddSqlTemplate("crontablist", crontablist); err != nil {
		panic(err)
	}

	hostlist := "select * from `host` where `id`!=''" +
		"{% if name %}" +
		" and `name` like '%{{name}}%'" +
		"{% endif %}" +
		"{% if hostuuid %}" +
		" and `hostuuid` like '%{{hostuuid}}%'" +
		"{% endif %}" +
		"{% if hostaddr %}" +
		" and `hostaddr` like '%{{hostaddr}}%'" +
		"{% endif %}" +
		"{% if pageSize && currentPage%}" +
		" limit {{pageSize*(currentPage-1)}},{{pageSize}} " +
		"{% endif %}"

	if err := localdb.SqlTemplate.AddSqlTemplate("hostlist", hostlist); err != nil {
		panic(err)
	}

}
